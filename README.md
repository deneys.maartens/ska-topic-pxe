# SKA-TOPIC-PXE

PXE configuration to install the temporary development k8s cluster at the TOPIC
offices.

## Requirements

- `dnsmasq` -- if using Ubuntu as a PXE server, only the `dnsmasq-base` package
  is required -- there is no need to start a full-fledged PXE service.
- `python3` -- used as a minimal HTTP server to serve `/srv/tftp` as directory
  listing

## User-configurable parameters

- `pxe.sh`:`NIC` -- the network interface to bind
- `pxe.sh`:`RELEASE` -- the Ubuntu version to serve

## Usage

Run `./pxe.sh`. You are likely to be prompted for your `sudo` password. `root`
perms are required to create `/srv/tftp/` from which the PXE files are served.
The `dnsmasq` process runs in the foreground, so after the requisite machine
has been installed, hit `CTRL-C` to stop the script. This _should_ also kill
the python-based web service.

The requisite files to allow the Ubuntu-20.04 Server installation media to be
net-booted are downloaded/extracted if they do not already exist in the
`/srv/tftp/` directory. The python built-in web-server is started on port 8000,
and a `dnsmasq` instance is started to provide PXE-booting capability.

During installation, some host-specific configuration is done, based on the
initially PXE-assigned IP address. These scripts are intended to seamlessly
integrate with the TOPIC office network configuration -- the only requirement
being that the hosts be isolated to allow PXE-booting off of _this_ PXE
service, and not the general office PXE service. Afterwards the hosts can be
connected to the office network for access in the accustomed way.

PXE-booting happens based on recognized MAC-addresses. All unrecognised
PXE-boot requests are ignored. Adjust the `pxe.sh` MAC-address list as
required.
