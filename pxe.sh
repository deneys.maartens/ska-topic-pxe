#! /bin/sh -e
#
# user-defined DHCP-service

RELEASE=22.04.1
NIC=enp1s0
#NIC=eno1

DNSMASQ=dnsmasq
IP=$(hostname -I | cut -d ' ' -f 1)
ISO=ubuntu-${RELEASE}-live-server-amd64.iso
TFTPROOT=/srv/tftp
TRAPSPEC="0 1 2 3 6 9 15"  # EXIT SIGHUP SIGINT SIGQUIT SIGABRT SIGKILL SIGTERM

mkdir -p $TFTPROOT

[ -e $TFTPROOT/$ISO ] || {
  fin (){
    umount $TFTPROOT/_ || :
    rmdir $TFTPROOT/_
  }
  trap fin $TRAPSPEC
  mkdir -p $TFTPROOT/_

  wget -P $TFTPROOT/ -q https://releases.ubuntu.com/${RELEASE}/"$ISO"
  mount -o loop $TFTPROOT/"$ISO" $TFTPROOT/_

  cd $TFTPROOT
  cp _/casper/vmlinuz _/casper/initrd $TFTPROOT/
  cd -

  fin
}

[ -e $TFTPROOT/lpxelinux.0 ] || {
  TMP=$(mktemp -d /tmp/pxelinux.XXXXXXXX) || exit 1
  fin (){
    rm -rf -- $TMP
  }
  trap fin $TRAPSPEC

  cd $TMP
  apt-get download pxelinux > /dev/null
  dpkg-deb --fsys-tarfile pxelinux_*.deb |
    tar x ./usr/lib/PXELINUX/lpxelinux.0 -O \
    > $TFTPROOT/lpxelinux.0
  cd -

  fin
}

[ -e $TFTPROOT/syslinux.e64 ] || {
  TMP=$(mktemp -d /tmp/syslinux.XXXXXXXX) || exit 1
  fin (){
    rm -rf -- $TMP
  }
  trap fin $TRAPSPEC

  cd $TMP
  apt-get download syslinux-efi > /dev/null
  dpkg-deb --fsys-tarfile syslinux-efi_*.deb |
    tar x ./usr/lib/SYSLINUX.EFI/efi32/syslinux.efi -O \
    > $TFTPROOT/syslinux.e32
  dpkg-deb --fsys-tarfile syslinux-efi_*.deb |
    tar x ./usr/lib/SYSLINUX.EFI/efi64/syslinux.efi -O \
    > $TFTPROOT/syslinux.e64
  cd -

  fin
}

[ -e $TFTPROOT/ldlinux.e64 ] || {
  TMP=$(mktemp -d /tmp/syslinux-common.XXXXXXXX) || exit 1
  fin (){
    rm -rf -- $TMP
  }
  trap fin $TRAPSPEC

  cd $TMP
  apt-get download syslinux-common > /dev/null
  dpkg-deb --fsys-tarfile syslinux-common_*.deb |
    tar x ./usr/lib/syslinux/modules/bios/ldlinux.c32 -O \
    > $TFTPROOT/ldlinux.c32
  dpkg-deb --fsys-tarfile syslinux-common_*.deb |
    tar x ./usr/lib/syslinux/modules/efi32/ldlinux.e32 -O \
    > $TFTPROOT/ldlinux.e32
  dpkg-deb --fsys-tarfile syslinux-common_*.deb |
    tar x ./usr/lib/syslinux/modules/efi64/ldlinux.e64 -O \
    > $TFTPROOT/ldlinux.e64
  cd -

  fin
}

mkdir -p $TFTPROOT/pxelinux.cfg
cat > $TFTPROOT/pxelinux.cfg/default <<!
DEFAULT install
LABEL install
  KERNEL vmlinuz
  INITRD initrd
  APPEND autoinstall cloud-config-url= ds=nocloud-net;s=http://$IP:8000/ fsck.mode=skip ip=dhcp url=http://$IP:8000/$ISO
!

cat > $TFTPROOT/meta-data <<!
!

# Host-specific configuration, with a static PXE service, is eh.. not entirely
# straight-forward -- some fancy footwork is involved.
# When the target machine first boots with PXE, the assigned IP address can
# form the basis of assigning the "role" of the target machine should fulfil --
# but the host-specific configuration can only happen during the first boot up
# _after_ the unattended installation has completed. When the machine boots up
# at this point, the DHCP-assigned IP address does not necessarily have to
# match the PXE-assigned IP address.
#
# So the host installation happens in 3 stages, namely:
# - PXE boot, which boots the target machine from the installation media;
# - this triggers cloud-init which performs an unattended installation,
# - and then final configuration happens when the target machine first boots.
#
# The host-specific configuration that happens during these stages are:
# - the host specific files are created very early during the unattended
#   installation process in /root/, even before /target/ is mounted, so we
#   can't create the files directly on the installation target
# - the /root/* files are copied to the installation target in the
#   `late-commands`, as defined in the `user-data` file: two files go into
#   /target/root/ and one file goes into /target/etc/cloud-init/cloud-config.d/
# - after the installation completes the target machine automatically reboots
# - during the first boot of the target machine, cloud-init is activated; the
#   user -- with details defined in the `autoinstall`/`identity` section of the
#   `user-data` file -- is created as part of cloud-init; in addition, the file
#   copied into /etc/cloud/cloud.cfg.d/ is executed, which triggers the
#   host-specific configuration based on the original IP address assigned
#   during the PXE-boot phase
cat > $TFTPROOT/user-data <<'!'
#cloud-config

autoinstall:
  apt:
    geoip: false
    primary:
      - arches: [default]
        uri: http://nl.archive.ubuntu.com/ubuntu
  identity:
    hostname: localhost
    password: $6$4aY.eDMI0qB.8XAE$BsRvOmped5y1L8Dg1zUP0lTY/cfI3V..TX7xGsLSjS/xjD0oIakL5vX6B./MEJpQFDoESyR/JZ3mDrIWPaegZ0
    username: ubuntu
  late-commands:
    - cp /root/ip /root/skao-cfg.sh /target/root/
    - cp /root/10-skao.cfg /target/etc/cloud/cloud.cfg.d/
  ssh:
    authorized-keys:
      - ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAINxD5lGkHUjy+/UB+zLQ4JCtbU4bDDAjYlq2ouSnMSXQ deneys.maartens@topic.nl
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDPfPgyEZZ5JLNxu/E+/o/ksdgqiNivCgsvhNlUeobbCPpIksv3z+KIRekrPJR7CAWnuySRghEAu/+EK+bq93tAr/ZUCWAhZmsBfLCQk5aWBIGaYXAaykXSumglfzogF+E9oeKqDyUv8XtBm7hOiZZlQ6jzFCxlHNDwaAgbbUEN0WmW3C6yhFVI+Db2VzgBLV1k7t9emiUatGz2s9QkEk3Z+xgF8Sd3fH/PmAl8PCMc8xWPV5Q/eUnXY9kmwigJ7NPp+a+rjkCauznttLG2d8vetZp3SSQitZCl0RFpvFSLnV19EQi31UDCnT3FI8nEOCi1WJSfLTLs3cqzvD/xTzXn sander.ploegsma@topic.nl
      - ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMcKnFyPRHA8+0vvVUwkNZXOh6CbdgFbblmWd/wREzg/afO9FB+v0ixL7FCEgyekAVwJfZdSPmPCrBNyepzVo22XX2eCp0QHefUB30EZCopPesW2VPwk9Ay+1ZMr3uzvUie21ofqZT2zs086EzyhZ2iY+22XqC5bS0L1mutK1chMfUcZM3iVDnXYckeab0u72Ce/QWXcQ+32lv2IndofzpT0ekFFEO77LcuoWFWkLjbtgI/ZnMcU1Vl0QAVxdvgXMyUZ8Ul89o3G/h/FhTbVJUN/sN6cdfn3H4aKEoAeCniq3fiJPn7pnd7c22Q0oVAA/WHd6zPyiZKGpnnR8qxsk1
    install-server: true
  storage:
    layout:
      name: direct
  user-data:
    users:
      - name: ubuntu
        sudo: ALL=(ALL) NOPASSWD:ALL
  version: 1

# The steps below runs before /target is mounted, so we can't write to the preferred destination in one step.

runcmd:
- ip -br a > /root/ip

write_files:
- path: /root/10-skao.cfg
  content: |
    runcmd:
    - sh -e /root/skao-cfg.sh && rm -f /root/skao-cfg.sh

- path: /root/skao-cfg.sh
  content: |
    #! /bin/sh -e
    #
    # local system config

    grep -q '\<192.168.88.100\>' /root/ip && {
      # cp -- control plane
      hostnamectl set-hostname 'skao0'
      sed -i'' -e '/dhcp4/a \      addresses: [192.168.88.100/24]' /etc/netplan/00-installer-config.yaml
      netplan apply
    }

    grep -q '\<192.168.88.1\>' /root/ip && {
      # w1 -- worker node
      hostnamectl set-hostname 'skao1'
      ## temporarily the gateway too; i.e. *.88.251
      sed -i'' -e '/dhcp4/a \      addresses: [192.168.88.1/24]' /etc/netplan/00-installer-config.yaml
      netplan apply
    }

    grep -q '\<192.168.88.2\>' /root/ip && {
      # w2 -- worker node
      hostnamectl set-hostname 'skao2'
      sed -i'' -e '/dhcp4/a \      addresses: [192.168.88.2/24]' /etc/netplan/00-installer-config.yaml
      netplan apply
    }

    grep -q '\<192.168.88.90\>' /root/ip && {
      # w2 -- worker node
      hostnamectl set-hostname 'skao-alveo'
      sed -i'' -e '/dhcp4/a \      addresses: [192.168.88.90/24]' /etc/netplan/00-installer-config.yaml
      netplan apply
    }

    cat >> /etc/hosts <<!

    192.168.88.100  skao0.topic.local skao0
    192.168.88.1    skao1.topic.local skao1
    192.168.88.2    skao2.topic.local skao2
    192.168.88.90   skao-alveo.topic.local skao-alveo
    !

    rm -f /root/ip
    rm -f /etc/cloud/cloud.cfg.d/10-skao.cfg

    # -fin-
# -fin-
!

cat > $TFTPROOT/vendor-data <<!
!

python3 -m http.server --directory $TFTPROOT & pypid=$!
fin (){
  kill -9 $pypid
}
trap fin $TRAPSPEC

sudo \
  $DNSMASQ \
    --bind-interfaces \
    --bogus-priv \
    --dhcp-boot=tag:bios,lpxelinux.0 \
    --dhcp-boot=tag:efi-x86,syslinux.e32 \
    --dhcp-boot=tag:efi-x86_64,syslinux.e64 \
    --dhcp-host=64:31:50:3a:87:a0,192.168.88.100,cp \
    --dhcp-host=e8:39:35:4e:d0:1d,192.168.88.1,w1 \
    --dhcp-host=90:b1:1c:88:1b:aa,192.168.88.2,w2 \
    --dhcp-host=f0:79:59:5a:81:c4,192.168.88.90,skao-alveo \
    --dhcp-ignore=tag:!known \
    --dhcp-match=set:bios,option:client-arch,0 \
    --dhcp-match=set:efi-x86,option:client-arch,6 \
    --dhcp-match=set:efi-x86_64,option:client-arch,7 \
    --dhcp-match=set:efi-x86_64,option:client-arch,9 \
    --dhcp-range=$IP,static,infinite \
    --enable-tftp \
    --interface=$NIC \
    --local=/skao.home.arpa/ \
    --no-daemon \
    --no-hosts \
    --no-resolv \
    --server=1.1.1.1 \
    --server=1.0.0.1 \
    --tftp-root=$TFTPROOT

# -fin-
